package com.carlossant47.agendasql;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.carlossant47.agendasql.database.AgendaContactos;
import com.carlossant47.agendasql.database.Contacto;

import java.util.ArrayList;

public class ListaActivity extends AppCompatActivity {

    private AgendaContactos agendaContactos;
    private Button btnNuevo;
    private ArrayList<Contacto> contactos;
    private ContactosAdapter adapter;
    private ListView listContactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        this.agendaContactos = new AgendaContactos(this);
        loadContactos();
        adapter = new ContactosAdapter(this, R.id.lbNombre, R.layout.layout_contacto, contactos);


        listContactos = findViewById(R.id.listContactos);
        listContactos.setAdapter(adapter);
    }

    private void loadContactos()
    {
        agendaContactos.openDatabase();
        contactos = agendaContactos.allContactos();
        agendaContactos.close();
    }

    class ContactosAdapter extends ArrayAdapter<Contacto> {
        private Context context;
        private int textViewResourceID;
        private ArrayList<Contacto> contactos;
        private LayoutInflater inflater;

        public ContactosAdapter(Context context, @IdRes int idTextView, @LayoutRes int  layoutIdResource, ArrayList<Contacto> items)
        {
            super(context, idTextView, items);
            this.context = context;
            this.textViewResourceID = layoutIdResource;
            this.contactos = items;
            this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = this.inflater.inflate(this.textViewResourceID, parent, false);
            TextView lbNombre  = view.findViewById(R.id.lbNombre);
            TextView lbTelefono  = view.findViewById(R.id.lbTelefono);
            Button btnEditar = view.findViewById(R.id.btnModificar);
            Button btnEliminar = view.findViewById(R.id.btnEliminar);

            lbNombre.setTextColor(contactos.get(position).isFavorito() ? Color.BLUE : Color.BLACK);
            lbTelefono.setTextColor(contactos.get(position).isFavorito() ? Color.BLUE : Color.BLACK);

            lbNombre.setText(contactos.get(position).getNombre());
            lbTelefono.setText(contactos.get(position).getTelefono());
            btnEliminar.setOnClickListener(v -> {
                agendaContactos.openDatabase();
                agendaContactos.deleteContacto(contactos.get(position).getId());
                agendaContactos.close();
                contactos.remove(position);

                notifyDataSetChanged();
            });

            btnEditar.setOnClickListener(v -> {
                Bundle bundle = new Bundle();
                bundle.putSerializable("contacto", contactos.get(position));
                Intent intent = new Intent();
                intent.putExtras(bundle);
                setResult(Activity.RESULT_OK, intent);
                finish();
            });


            return view;
        }
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            return getView(position, convertView, parent);
        }
    }
}
