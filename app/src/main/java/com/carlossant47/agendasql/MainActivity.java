package com.carlossant47.agendasql;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.carlossant47.agendasql.database.AgendaContactos;
import com.carlossant47.agendasql.database.AgendaDBHelper;
import com.carlossant47.agendasql.database.Contacto;

public class MainActivity extends AppCompatActivity {

    private AgendaContactos db;
    private EditText txtNombre;
    private EditText txtTel1;
    private EditText txtTel2;
    private EditText txtDomicilio;
    private EditText txtNotas;
    private CheckBox cbFavorito;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnLista;
    private Button btnSalir;
    private Contacto saveContact = null;
    private long ID;
    private boolean isEdit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new AgendaContactos(this);
        txtNombre = findViewById(R.id.txtNombre);
        txtTel1 = findViewById(R.id.txtTel1);
        txtTel2 = findViewById(R.id.txtTel2);
        txtDomicilio = findViewById(R.id.txtDomicilio);
        txtNotas = findViewById(R.id.txtNotas);
        cbFavorito = findViewById(R.id.cbFavorito);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLista = findViewById(R.id.btnLista);
        btnSalir = findViewById(R.id.btnSalir);

        btnGuardar.setOnClickListener(this.btnGuardarAction());
        btnLista.setOnClickListener(this.btnListarAction());
        this.btnLimpiar.setOnClickListener(this.btnLimpiarAction());
        this.btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private View.OnClickListener btnLimpiarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        };
    }
    private View.OnClickListener btnListarAction()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListaActivity.class);
                startActivityForResult(intent, 0);
            }
        };
    }


    private View.OnClickListener btnGuardarAction() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDatabase();
                if (validar()) {
                    Contacto contacto = new Contacto();
                    contacto.setNombre(getText(txtNombre));
                    contacto.setDireccion(getText(txtDomicilio));
                    contacto.setNotss(getText(txtNotas));
                    contacto.setTelefono(getText(txtTel1));
                    contacto.setTelefono2(getText(txtTel2));
                    contacto.setFavorito(cbFavorito.isChecked());
                    if(!isEdit)
                    {
                        if (db.insertContacto(contacto) != -1)
                            Toast.makeText(MainActivity.this, "Se agrego correctamente", Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(MainActivity.this, "Se produjo un error", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        contacto.setId(ID);
                        db.updateContacto(contacto);
                        Toast.makeText(MainActivity.this, "Se actualizo correctamente", Toast.LENGTH_SHORT).show();
                    }



                    limpiar();
                }
                else
                    Toast.makeText(MainActivity.this, R.string.msgerror, Toast.LENGTH_SHORT).show();
                db.close();
            }
        };
    }

    private void limpiar(){
        txtDomicilio.setText("");
        txtNombre.setText("");
        txtTel1.setText("");
        txtTel2.setText("");
        txtNotas.setText("");
        cbFavorito.setChecked(false);
        isEdit = false;
        saveContact = null;
        ID = 0;
    }
    private boolean validar(){
        if(validarCampo(txtDomicilio) || validarCampo(txtNombre) ||
                validarCampo(txtNotas) || validarCampo(txtTel1) || validarCampo(txtTel2))
            return false;
        else
            return true;
    }

    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(Activity.RESULT_OK == resultCode)
        {
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            saveContact = contacto;
            ID = saveContact.getId();
            txtNombre.setText(saveContact.getNombre());
            txtNotas.setText(saveContact.getNotss());
            txtTel1.setText(saveContact.getTelefono());
            txtTel2.setText(saveContact.getTelefono2());
            txtDomicilio.setText(saveContact.getDireccion());
            cbFavorito.setChecked(saveContact.isFavorito());
            isEdit = true;

        }
        else
        {
            limpiar();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
