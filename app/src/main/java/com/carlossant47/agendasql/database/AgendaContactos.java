package com.carlossant47.agendasql.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaContactos {
    private Context context;
    private AgendaDBHelper helper;
    private SQLiteDatabase db;

    private String[] columns =
            {DefinirTabla.ID, DefinirTabla.NOMBRE, DefinirTabla.TELEFONO1,
                    DefinirTabla.TELEFONO2, DefinirTabla.NOTAS, DefinirTabla.DIRECCION, DefinirTabla.FAVORITO};

    public AgendaContactos(Context context)
    {
        this.context = context;
        this.helper = new AgendaDBHelper(context);

    }

    public void openDatabase()
    {
        db = helper.getWritableDatabase();
    }

    public long insertContacto(Contacto contacto)
    {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.NOMBRE, contacto.getNombre());
        values.put(DefinirTabla.TELEFONO1, contacto.getTelefono());
        values.put(DefinirTabla.TELEFONO2, contacto.getTelefono2());
        values.put(DefinirTabla.DIRECCION, contacto.getDireccion());
        values.put(DefinirTabla.NOTAS, contacto.getNotss());

        values.put(DefinirTabla.FAVORITO, contacto.isFavorito() ? 1 : 0);
        return db.insert(DefinirTabla.TABLE_CONTACTO, null, values);

    }

    public long updateContacto(Contacto contacto)
    {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.NOMBRE, contacto.getNombre());
        values.put(DefinirTabla.TELEFONO1, contacto.getTelefono());
        values.put(DefinirTabla.TELEFONO2, contacto.getTelefono2());
        values.put(DefinirTabla.DIRECCION, contacto.getDireccion());
        values.put(DefinirTabla.FAVORITO, contacto.isFavorito() ? 1 : 0);
        String where = DefinirTabla.ID + "=" + contacto.getId();
        return db.update(DefinirTabla.TABLE_CONTACTO, values, where, null);
    }

    public long deleteContacto(long id)
    {
        String where = DefinirTabla.ID + "=" + id;
        return db.delete(DefinirTabla.TABLE_CONTACTO, where, null);

    }

    private Contacto readContacto(Cursor cursor)
    {
        Contacto contacto = new Contacto();
        contacto.setId(cursor.getInt(0));
        contacto.setNombre(cursor.getString(1));
        contacto.setTelefono(cursor.getString(2));
        contacto.setTelefono2(cursor.getString(3));
        contacto.setNotss(cursor.getString(4));
        contacto.setDireccion(cursor.getString(5));
        contacto.setFavorito(cursor.getInt(6) == 1);
        return contacto;

    }

    public Contacto getContacto(long id)
    {
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.TABLE_CONTACTO,columns,
                DefinirTabla.ID + " = ?" , new String[]{String.valueOf(id)},null,
                null, null);
        cursor.moveToFirst();
        Contacto contacto =this.readContacto(cursor);
        cursor.close();
        return contacto;
    }

    public ArrayList<Contacto> allContactos()
    {
        ArrayList<Contacto> contactos = new ArrayList<>();
        Cursor cursor = db.query(DefinirTabla.TABLE_CONTACTO, columns, null,
                null, null, null,null);
        if(cursor!= null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                contactos.add(readContacto(cursor));
                cursor.moveToNext();
            }
            cursor.close();

        }


        return contactos;

    }

    public void close()
    {
        helper.close();
    }
}
