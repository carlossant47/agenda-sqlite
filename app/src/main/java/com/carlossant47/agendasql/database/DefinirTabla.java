package com.carlossant47.agendasql.database;

public abstract class DefinirTabla {
    public final static String ID = "idcontacto";
    public final static String NOMBRE = "nombre";
    public final static String TELEFONO1 = "telefono1";
    public final static String TELEFONO2 = "telefon2";
    public final static String NOTAS = "notas";
    public final static String DIRECCION = "direccion";
    public final static String FAVORITO = "favorito";
    public final static String TABLE_CONTACTO = "contacto";

}
